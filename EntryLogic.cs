﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EntryState
{
    Active,
    Inactive
}

public class EntryLogic : MonoBehaviour
{
    int m_KeyNum;
    EntryState m_EntryState;
    // Start is called before the first frame update
    void Start()
    {
        m_EntryState = EntryState.Inactive;
    }

    // Update is called once per frame
    void Update()
    {
        if (m_KeyNum == 3)
        {
            m_EntryState = EntryState.Active;
        }
    }
    public void GetKey()
    {
        m_KeyNum += 1;
    }
    public void Save()
    {
        PlayerPrefs.SetInt("KeyNumber",m_KeyNum);
        PlayerPrefs.SetInt("EntryState", (int)m_EntryState);
    }
    public void Load()
    {
        m_KeyNum = PlayerPrefs.GetInt("KeyNumber");
        m_EntryState = (EntryState)PlayerPrefs.GetInt("EntryState");
    }
}
