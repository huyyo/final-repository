﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class LightLogic : MonoBehaviour
{
    // Start is called before the first frame update
    Light m_light;
    [SerializeField]
    float IntensityDecline;
    [SerializeField]
    float BrightTime;
    [SerializeField]
    float DarkTime;
    bool bright;
    float m_time;
    void Start()
    {
        m_light = GetComponent<Light>();
        bright = true;
    }
    // Update is called once per frame
    void Update()
    {
        m_time -= Time.deltaTime;
        if(m_time < 0)
        {
            if(bright)
            {
                SetDark();
            }
            else
            {
                SetBright();
            }
        }
    }
    void SetDark()
    {
        bright = false;
        m_time = DarkTime;
        m_light.intensity -= IntensityDecline;
    }
    void SetBright()
    {
        bright = true;
        m_time = BrightTime;
        m_light.intensity += IntensityDecline;
    }
}
