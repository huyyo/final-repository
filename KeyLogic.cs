﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum KeyState
{
    Active,
    Inactive
}

public class KeyLogic : MonoBehaviour
{
    MeshRenderer m_meshRenderer;
    Collider m_collider;
    public KeyState m_State ;
    // Start is called before the first frame update
    void Start()
    {
        m_meshRenderer = GetComponent<MeshRenderer>();
        m_collider = GetComponent<Collider>();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetState(KeyState state) 
    {
        m_State = state;
        m_meshRenderer.enabled = m_State == KeyState.Active;
        m_collider.enabled = m_State == KeyState.Active;
    }
    public void Save()
    {
        PlayerPrefs.SetInt("KeyState", (int)m_State);
    }

    public void Load()
    {
        
        SetState((KeyState)PlayerPrefs.GetInt("KeyState"));
        
    }
    void DoSomething()
    {
        SetState(KeyState.Inactive);
    }
    

}
