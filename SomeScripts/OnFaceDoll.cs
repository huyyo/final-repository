﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum OnFaceDollState
{
    Active,
    Inactive
    }


public class OnFaceDoll : MonoBehaviour
{
    [SerializeField]
    float m_remainedTime;
    bool visible;
    MeshRenderer m_meshRenderer;
    Collider m_collider;
    GameObject m_player;
    OnFaceDollState m_dollState;
    // Start is called before the first frame update
    void Start()
    {
        m_collider = GetComponent<Collider>();
        m_meshRenderer = GetComponent<MeshRenderer>();
        m_collider.enabled = false;
        m_meshRenderer.enabled = false;
        m_dollState = OnFaceDollState.Inactive;
    }

    // Update is called once per frame
    void Update()
    {
        if(m_dollState == OnFaceDollState.Active)
        {
            UpdateTime();
        }
    }
    void UpdateTime()
    {
        if (m_remainedTime < 0)
        {
            m_collider.enabled = false;
            m_meshRenderer.enabled = false;
            m_dollState = OnFaceDollState.Inactive;
            return;
        }
        else
        {
            m_remainedTime -= Time.deltaTime;

        }
    }
    public void GetVisible()
    {
        m_dollState = OnFaceDollState.Active;
        m_collider.enabled = true;
        m_meshRenderer.enabled = true;
    }
    public void Save(string str)
    {

        PlayerPrefs.SetInt("OnFaceDollState" + str, (int)m_dollState);
    }
    public void Load(string str)
    {
        m_dollState = (OnFaceDollState)PlayerPrefs.GetInt("OnFaceDollState" + str);
    }
}
