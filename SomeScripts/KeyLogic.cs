﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum KeyState
{
    Active,
    Inactive
}


public class KeyLogic : MonoBehaviour
{

    GameObject m_player;
    Player_Logic m_playerLogic;
    MeshRenderer m_meshRenderer;
    Collider m_collider;
    public KeyState m_State;
    // Start is called before the first frame update
    void Start()
    {
        m_meshRenderer = GetComponentInChildren<MeshRenderer>();
        m_collider = GetComponent<Collider>();
        m_player = GameObject.FindGameObjectWithTag("Player");
        m_State = KeyState.Active;
        m_playerLogic = m_player.GetComponent<Player_Logic>();
    }

    // Update is called once per frame
    void Update()
    {
        if(m_State == KeyState.Active)
        {
            DoSomething();
        }
    }
    public void SetState(KeyState state)
    {
        m_State = state;
        m_meshRenderer.enabled = m_State == KeyState.Active;
        m_collider.enabled = m_State == KeyState.Active;
    }
    public int ReturnState()
    {
        return (int) m_State;
    }
    void DoSomething()
    {
        if(m_State == KeyState.Active)
        {
            SetState(KeyState.Inactive);
            m_playerLogic.GetKey();
        }
    }
    public void Save(string str)
    {
        PlayerPrefs.SetInt("KeyState" + str, (int)m_State);
    }

    public void Load(string str)
    {
        SetState((KeyState)PlayerPrefs.GetInt("KeyState" + str));
    }
}
