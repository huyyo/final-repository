﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager m_instance;
    public static GameManager Get() => m_instance;
    // Start is called before the first frame update
    GameObject[] m_keys;
    List<KeyLogic> m_keyLogics = new List<KeyLogic>();
    GameObject[] m_doors;
    GameObject m_player;
    List<DoorLogic> m_doorLogics = new List<DoorLogic>();
    Player_Logic m_playerLogic;
    GameObject[] m_dolls;
    List<DollLogic> m_dollLogics = new List<DollLogic>();
    GameObject m_OnFaceDoll;
    OnFaceDoll m_onFaceDoll;
    string Record;
    void Awake()
    {
        if(m_instance == null)
        {
            m_instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        Record = "The record 0 ";
        Origin();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Save(string str)
    {
        for (int index = 0; index < m_keys.Length; index++)
        {
            m_keyLogics[index].Save(str + index);
        }
            
        for (int index = 0; index < m_doors.Length; index++)
        {
            m_doorLogics[index].Save(str + index);
        }
        m_playerLogic.Save(str);
        m_onFaceDoll.Save(str);
        for (int index = 0; index < m_dolls.Length; index++)
        {
            m_dollLogics[index].Save(str);
        }

    }

    public void Load(string str)
    {
        for (int index = 0; index < m_keys.Length; index++)
        {
            m_keyLogics[index].Load(str + index);
        }
        for (int index = 0; index < m_doors.Length; index++)
        {
            m_doorLogics[index].Load(str + index);
        }
        m_playerLogic.Load(str);
        m_onFaceDoll.Load(str);
        for (int index = 0; index < m_dolls.Length; index ++)
        {
            m_dollLogics[index].Load(str);
        }
    }
    void Origin()
    {
        m_keys = GameObject.FindGameObjectsWithTag("Keys");
        for (int index = 0; index < m_keys.Length; index++)
        {
            m_keyLogics.Add(m_keys[index].GetComponent<KeyLogic>());
        }
        m_doors = GameObject.FindGameObjectsWithTag("Doors");
        for (int index = 0; index < m_keys.Length; index++)
        {
            m_doorLogics.Add(m_doorLogics[index].GetComponent<DoorLogic>());
        }
        m_player = GameObject.FindGameObjectWithTag("Player");
        m_playerLogic = m_player.GetComponent<Player_Logic>();
        m_dolls = GameObject.FindGameObjectsWithTag("Doll");
        for (int index = 0; index < m_dolls.Length;index ++)
        {
            m_dollLogics.Add(m_dolls[index].GetComponent<DollLogic>());
        }
        m_OnFaceDoll = GameObject.FindGameObjectWithTag("Doll");
        m_onFaceDoll = m_OnFaceDoll.GetComponent<OnFaceDoll>();
        PlayerPrefs.Save();
        Save(Record);
    }
    public void SetName(string str)
    {
        Record = str;
    }
}
