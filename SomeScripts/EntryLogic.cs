﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EntryState
{
    Active,
    Inactive
}

public class EntryLogic : MonoBehaviour
{
    int CanOpen;
    int m_KeyNum;
    EntryState m_EntryState;
    GameObject m_player;
    Player_Logic m_playerLogic;
    // Start is called before the first frame update
    void Start()
    {
        m_EntryState = EntryState.Inactive;
        CanOpen = 0;
        m_player = GameObject.FindGameObjectWithTag("player");
        m_playerLogic = m_player.GetComponent<Player_Logic>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void Save(string str)
    {
        
        PlayerPrefs.SetInt("EntryState" + str, (int)m_EntryState);
        PlayerPrefs.SetInt("CanOpen" + str, CanOpen);
    }
    public void Load(string str)
    {
        
        CanOpen = PlayerPrefs.GetInt("CanOpen", CanOpen);
        if(m_EntryState != (EntryState)PlayerPrefs.GetInt("EntryState" + str))
        {
            SetState();
        }
    }
    void SetState()
    {
        switch (m_EntryState)
        {
            case (EntryState.Active):
                transform.Rotate(new Vector3(0, 90, 0));
                m_EntryState = EntryState.Inactive;
                break;
            case (EntryState.Inactive):
                transform.Rotate(new Vector3(0, -90, 0));
                m_EntryState = EntryState.Active;
                break;
        }
    }
    public void OpenEntryDoor()
    {
        if (CanOpen == 0)
        {
            if (m_playerLogic.ReturnKeyNum() > 0)
            {
                m_playerLogic.UseKey();
                CanOpen = 1;
            }
            else
                return;
        }
        SetState();    
        
        
    }
}
