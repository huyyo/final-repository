﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum DollState
{
    Active,
    Inactive
}
public class DollLogic : MonoBehaviour
{
    MeshRenderer m_meshRenderer;
    Collider m_collider;
    GameObject m_player;
    GameObject m_doll;
    OnFaceDoll m_dollLogic;
    Camera m_camera;
    DollState m_dollState;
    // Start is called before the first frame update
    void Start()
    {
        m_collider = GetComponent<Collider>();
        m_meshRenderer = GetComponent<MeshRenderer>();
        m_doll = GameObject.FindGameObjectWithTag("OnFaceDoll");
        m_dollLogic = m_doll.GetComponent<OnFaceDoll>();
        m_camera = Camera.main;
        m_meshRenderer.enabled = true;
        m_collider.enabled = true;
        m_dollState = DollState.Active;
    }

    // Update is called once per frame
    void Update()
    {
        if(FindPlayer() && m_dollState == DollState.Active)
        {
            m_dollLogic.GetVisible();
            m_dollState = DollState.Inactive;
            m_meshRenderer.enabled = false;
            m_collider.enabled = false;
        }
    }

    public bool FindPlayer( )
    {
        Vector3 targetObjViewportCoord = m_camera.WorldToViewportPoint(m_player.transform.position);
        print("x:");
        print(targetObjViewportCoord.x);
        print("y:");
        print(targetObjViewportCoord.y);
        print("z:");
        print(targetObjViewportCoord.z);
        if (targetObjViewportCoord.x > 0 && targetObjViewportCoord.x < 1 && targetObjViewportCoord.y > 0f && targetObjViewportCoord.y < 1 && targetObjViewportCoord.z > m_camera.nearClipPlane && targetObjViewportCoord.z < 5.0f)
        {
            return true;
        }
        return false;
    }

    public void Save(string str)
    {
        PlayerPrefs.SetInt("DollState" + str, (int)m_dollState);
    }
    public void Load(string str)
    {

        m_dollState =(DollState) PlayerPrefs.GetInt("DollState" + str);
    }

}
