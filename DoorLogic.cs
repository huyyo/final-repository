﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DoorState
{
    Active,
    Inactive
}

public class DoorLogic : MonoBehaviour
{

    [SerializeField]
    DoorState m_doorState = DoorState.Inactive;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetState()
    {
        switch(m_doorState)
        {
            case (DoorState.Active):
                transform.Rotate(new Vector3(0, 90, 0));
                m_doorState = DoorState.Inactive;
                break;
            case (DoorState.Inactive):
                transform.Rotate(new Vector3(0, -90, 0));
                m_doorState = DoorState.Active;
                break;
        }
    }
    public void Save()
    {
        PlayerPrefs.SetInt("DoorState", (int)m_doorState);
    }
    public void Load()
    {
        if(m_doorState != (DoorState)PlayerPrefs.GetInt("DoorState"))
        {
            SetState();
        }
    }
}
